from base_entity import Entity
from utils.math import Vec2
from settings import TILE_WIDTH

class MovingPlatform(Entity):
    def __init__(self, xml_obj, tilemap):
        Entity.__init__(self, xml_obj, tilemap)
        self.xspeed = int(self.properties.get('xspeed', None))
        self.yspeed = int(self.properties.get('yspeed', None))

        left_limit_in_tiles = int(self.properties.get('left_limit', None))
        right_limit_in_tiles = int(self.properties.get('right_limit', None))
        if left_limit_in_tiles != 0 and right_limit_in_tiles != 0:
            self.left_limit = self.rect.left - left_limit_in_tiles*TILE_WIDTH
            self.right_limit = self.rect.left + right_limit_in_tiles*TILE_WIDTH
        else:
            upper_limit_in_tiles = int(self.properties.get('upper_limit', None))
            lower_limit_in_tiles = int(self.properties.get('lower_limit', None))
            if upper_limit_in_tiles != 0 and lower_limit_in_tiles != 0:
                self.upper_limit = self.rect.top - int(self.properties.get('upper_limit', None))*TILE_WIDTH
                self.lower_limit = self.rect.top + int(self.properties.get('lower_limit', None))*TILE_WIDTH
        
        self.flag = True
        self.speed = Vec2(self.xspeed, self.yspeed)
        self.pos = Vec2(self.rect.topleft[0], self.rect.topleft[1])
        
    def update(self, ticks):
        # begin hack
        if hasattr(self, "left_limit") and hasattr(self, "right_limit"):
            if self.flag:
                # moving right
                if self.pos.x > self.right_limit:
                    self.flag = False
                    self.speed.x = -self.xspeed
            # moving left
            elif self.pos.x < self.left_limit:
                self.flag = True
                self.speed.x = self.xspeed
        elif hasattr(self, "upper_limit") and hasattr(self, "lower_limit"):
            if self.flag:
                # moving down
                if self.pos.y > self.lower_limit:
                    self.flag = False
                    self.speed.y = -self.yspeed
            # moving up
            elif self.pos.y < self.upper_limit:
                self.flag = True
                self.speed.y = self.yspeed
        # end hack
        self.pos += self.speed*ticks
        self.rect.topleft = self.pos.coords()
