import pygame
from base_entity import Entity

class Shooter(Entity):
    def __init__(self, xml_obj, tilemap, eventid):
        Entity.__init__(self, xml_obj, tilemap)

        self.direction = int(self.properties.get('direction', None))
        self.timer_in_ms = int(self.properties.get('timer', None))
        pygame.time.set_timer(eventid, self.timer_in_ms)
