import pygame
from pygame.locals import *
from settings import TILE_WIDTH, TILE_HEIGHT 
from base_entity import Entity

class OscillatingDeath(Entity):
    def __init__(self, xml_obj, tilemap):
        Entity.__init__(self, xml_obj, tilemap)
        
        upper_limit_in_tiles = int(self.properties.get('upper_limit', None))
        lower_limit_in_tiles = int(self.properties.get('lower_limit', None))
        if upper_limit_in_tiles:
            self.upper_limit = self.rect.top - upper_limit_in_tiles*TILE_WIDTH
        else:
            print "ERROR: Upper limit not defined for Oscillating Death object."
        if lower_limit_in_tiles:
            self.lower_limit = self.rect.top + lower_limit_in_tiles*TILE_HEIGHT
        else:
            print "ERROR: Lower limit not defined for Oscillatind Death object."

        self.speed = int(self.properties.get('speed', None))
        self.flag = True
        
    def update(self, ticks):
        """
        Upper and lower are wrt the player so upper y coordinate is actually lesser than the
        lower y coordinate.

        Moves up it till it reaches upper limit then moves down till it reaches lower limit, repeat.
        """
        if self.flag:
            self.rect.move_ip(0, -self.speed)
            if self.rect.top < self.upper_limit:
                self.flag = False
        else:
            self.rect.move_ip(0, self.speed)
            if self.rect.bottom > self.lower_limit:
                self.flag = True

class Bullet(Entity):
    def __init__(self, xml_obj, tilemap):
        Entity.__init__(self, xml_obj, tilemap)

        self.direction = int(self.properties.get("direction", None))
        self.speed = 6

    def update(self, ticks):
        self.rect.move_ip(self.direction*self.speed, 0)
