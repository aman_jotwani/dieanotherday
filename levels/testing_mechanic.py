import os, sys, pygame
from pygame.locals import *
from settings import BASE_DIR, SCREEN_WIDTH, SCREEN_HEIGHT
from entities.player import Player
from base_level import Level
from entities.platform import MovingPlatform

class MechTest(Level):
    def __init__(self, screen, tileset_path, tilemap_path, player):
        Level.__init__(self, screen, tileset_path, tilemap_path, player)

        # level-specific code
        mplat_width = 75
        mplat_height = 20
        mplat_speed = 100
        topleft = MovingPlatform(100, 100, mplat_width, mplat_height, (mplat_speed, mplat_speed), (1, 0))
        topright = MovingPlatform(600, 100, mplat_width, mplat_height, (mplat_speed, mplat_speed), (0, 1))
        bottomright = MovingPlatform(600, 600, mplat_width, mplat_height, (mplat_speed, mplat_speed), (-1, 0))
        bottomleft = MovingPlatform(100, 600, mplat_width, mplat_height, (mplat_speed, mplat_speed), (0, -1))
        mg = pygame.sprite.Group(topleft, topright, bottomright, bottomleft)
        self.add_moving_platforms(mg)

        pygame.time.set_timer(pygame.USEREVENT + 2, 5000)
        self.run()

    def run(self):
        while True:
            ticks = self.timer.tick(60)/1000.0
            for event in pygame.event.get():
                if event.type == pygame.QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                    self.level_complete()
                elif event.type == pygame.USEREVENT + 2:
                    for plat in self.moving_platforms:
                        if plat.direction.coords() == (1, 0):
                            plat.set_direction((0, 1))
                        elif plat.direction.coords() == (-1, 0):
                            plat.set_direction((0, -1))
                        elif plat.direction.coords() == (0, 1):
                            plat.set_direction((-1, 0))
                        elif plat.direction.coords() == (0, -1):
                            plat.set_direction((1, 0))
                else:
                    self.player.handle_events(event)
            
            self.player.update(ticks)  

            self.draw(self.screen)
            self.screen.blit(self.player.image, self.player.rect)
            
            if self.conn_platforms is not None:
                self.conn_platforms.draw(self.screen)
            
            if self.moving_platforms is not None:
                self.moving_platforms.update(ticks)
                self.moving_platforms.draw(self.screen)
            pygame.display.flip()

if __name__ == '__main__':
    pygame.init()
    screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
    tileset_path = os.path.join(BASE_DIR, "assets/images/generic_platformer_tiles.png")
    tilemap_path = os.path.join(BASE_DIR, "assets/tilemaps/revisedlevel1.tmx")
    dude = Player()
    dude.set_startpos(100, 50)
    level = MechTest(screen, tileset_path, tilemap_path, dude)
