import os, sys, pygame
from pygame.locals import *
from base_level import Level
from entities.player import Player
from entities.platform import MovingPlatform
from entities.shooter import Shooter
from entities.death import Bullet
from settings import BASE_DIR, SCREEN_WIDTH, SCREEN_HEIGHT

class Level4(Level):
    def __init__(self, screen, tileset_path, tilemap_path, player):
        Level.__init__(self, screen, tileset_path, tilemap_path, player)
        self.initialize_entities()

    def initialize_entities(self):
        Level.initialize_entities(self)

        s = Shooter(self.get_object("shooter", "shooter"), self, pygame.USEREVENT + 5) # add eventid?
        m = MovingPlatform(self.get_object("middle", "moving_platform"), self)
        k = MovingPlatform(self.get_object("key", "moving_key"), self)
        self.add(m, layer = 1)
        self.add(k, layer = 1)
        self.key = k
        self.add(s, layer = 1)
        self.moving_plats = [m]
        
    def run(self):
        while True:
            if self.level_running:
                ticks = self.timer.tick(60)/1000.0

                # handle events, delegate everything other than EXIT to player
                for event in pygame.event.get():
                    if event.type == pygame.QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                        self.level_complete(0)
                    # HANDLE SHOOTERS HERE
                    elif event.type == pygame.USEREVENT + 5:
                        # this is highly inefficient if level keeps running for long interval
                        bullet = Bullet(self.get_object("generic_bullet", "bullet"), self)
                        self.add(bullet, layer=0)
                        self.bullet = bullet
                    else:
                        self.player.handle_events(event)

                # the collision system
                self.handle_collision()

                # update the level and the player
                self.update(ticks)
                self.player.update(ticks)
                

                # clear the screen and redraw
                self.screen.fill((0, 0, 0, 1))
                self.draw(self.screen)
                self.screen.blit(self.player.image, self.player.rect)

                pygame.display.flip()


    def handle_collision(self):
        Level.handle_collision(self)

        # moving platform
        for m in self.moving_plats:
            if pygame.sprite.collide_rect(self.player, m):
                self.player.land_on_moving_platform(m)

        # bullets
        if hasattr(self, "bullet"):
            if pygame.sprite.collide_rect(self.player, self.bullet):
                self.player.bullet_struck(self.bullet, self.tombstone)
        
if __name__ != '__main__':
    pygame.init()
    screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT), pygame.FULLSCREEN)
    dude = Player()
    tileset_path = os.path.join(BASE_DIR, "assets/images/generic_platformer_tiles.png")
    tilemap_path = os.path.join(BASE_DIR, "assets/tilemaps/fourth.tmx")
    level = Level4(screen, tileset_path, tilemap_path, dude)
    level.run()
