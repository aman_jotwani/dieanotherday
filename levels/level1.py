import sys, os, pygame
from pygame.locals import *
from settings import BASE_DIR, SCREEN_WIDTH, SCREEN_HEIGHT
from entities.player import Player
from base_level import Level

class Level1(Level):
	def __init__(self, screen, tileset_path, data_path, player):
		Level.__init__(self, screen, tileset_path, data_path, player)
                self.initialize_entities()
                
if __name__ != '__main__':
	pygame.init()
	screen = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)
	dude = Player()
	level = Level1(screen, os.path.join(BASE_DIR, 'assets/images/generic_platformer_tiles.png'),
                       os.path.join(BASE_DIR, 'assets/tilemaps/first.tmx'), dude)
        level.run()
