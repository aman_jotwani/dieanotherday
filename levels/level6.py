import os, sys, pygame
from pygame.locals import *
from settings import BASE_DIR, SCREEN_WIDTH, SCREEN_HEIGHT
from base_level import Level
from entities.death import OscillatingDeath
from entities.player import Player

class Level6(Level):
    def __init__(self, screen, tileset_path, tilemap_path, player):
        Level.__init__(self, screen, tileset_path, tilemap_path, player)

        self.initialize_entities()

    def initialize_entities(self):
        # this could be shifted to base_level
        for t in self.sprites():
            Level.initialize_entities(self, t)
        pos = self.get_object("player_startpos", "position")
        self.player.set_startpos((int(pos.get('x')), int(pos.get('y'))))
        # uptil here
        
        left_death = OscillatingDeath(self.get_object("left", "oscillating_death"), self) 
        right_death = OscillatingDeath(self.get_object("right", "oscillating_death"), self)
        self.add(left_death, layer=1)
        self.add(right_death, layer=1)
        self.deadly.append(left_death)
        self.deadly.append(right_death)

    def handle_collision(self):
        Level.handle_collision(self)

if __name__ != '__main__':
    pygame.init()
    screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
    dude = Player()
    tileset_path = os.path.join(BASE_DIR, "assets/images/generic_platformer_tiles.png")
    tilemap_path = os.path.join(BASE_DIR, "assets/tilemaps/generic.tmx")
    level = Level6(screen, tileset_path, tilemap_path, dude)
    level.run()
