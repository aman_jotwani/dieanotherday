import os, sys, pygame
from pygame.locals import *
from settings import BASE_DIR, SCREEN_HEIGHT, SCREEN_WIDTH, TILE_WIDTH, TILE_HEIGHT 
from entities.player import Player
from base_level import Level
from entities.platform import MovingPlatform
from entities.death import OscillatingDeath
"""
Design idea -- each time you respawn from the tombstone, it moves, your respawn is the trigger.
"""
class Level3(Level):
	def __init__(self, screen, tileset_path, tilemap_path, player):
		Level.__init__(self, screen, tileset_path, tilemap_path, player)
                self.initialize_entities()
                
        def initialize_entities(self):
                Level.initialize_entities(self)
                # level-specific objects

                m = MovingPlatform(self.get_object("mp", "moving_platform"), self)
                d = OscillatingDeath(self.get_object("death", "oscillating_death"), self)
                self.add(m, layer=1)
                self.add(d, layer = 1)
                self.deadly.append(d.rect)
                self.moving_plats = [m]

        def handle_collision(self):
                Level.handle_collision(self)

                for coll in self.moving_plats:
                        if pygame.sprite.collide_rect(coll, self.player):
                                self.player.land_on_moving_platform(coll)                
      
if __name__ != '__main__':
	pygame.init()
	screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
	dude = Player()
	tileset_path = os.path.join(BASE_DIR, "assets/images/generic_platformer_tiles.png")
	tilemap_path = os.path.join(BASE_DIR, "assets/tilemaps/third.tmx")
	level = Level3(screen, tileset_path, tilemap_path, dude)
        level.run()
