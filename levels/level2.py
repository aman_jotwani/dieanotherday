import os, sys, pygame
from pygame.locals import *
from settings import BASE_DIR, SCREEN_WIDTH, SCREEN_HEIGHT, TILE_WIDTH, TILE_HEIGHT
from entities.player import Player
from utils.tiles import Tilemap
from base_level import Level
from entities.death import OscillatingDeath

class Level2(Level):
	def __init__(self, screen, tileset_path, data_path, player):
		Level.__init__(self, screen, tileset_path, data_path, player)
                self.initialize_entities()

if __name__ != '__main__':
	pygame.init()
	screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
	dude = Player()
	level = Level2(screen, os.path.join(BASE_DIR, 'assets/images/generic_platformer_tiles.png'),
				   os.path.join(BASE_DIR, 'assets/tilemaps/second.tmx'), dude)
        level.run()
